# git lab （ギットラボ）について

- git lab
[参照url](https://about.gitlab.com/)
- git lab 非公式日本語サイト
[参照url](https://www.gitlab.jp/)  
- git lab プラン別の機能  
（下記ページの各機能の下にオレンジ色のアイコンが並んでいる。「CORE」が社内サーバー無料のプラン。）  
[参照url](https://www.gitlab.jp/features/)
- git lab 注意点とノウハウ（2018/06版）  
Community Edition（CE）（gitlab-ce）は無料・無制限  
[参照url](https://techracho.bpsinc.jp/morimorihoge/2018_06_04/57628)   

<br>
<br>

# git lab ギットラボを使ってみよう

1. git lab のアカウント作成  
（一旦はGitLab.comのfreeプランでアカウント作成）  
2. SSH認証キーの作成  
以前、git hubにログインする際に作成したキーを使用します。  
再度、別の鍵を作る必要はないかと思ってます。  
[以前紹介した参照url](https://github.com/miyazaki-mba/git_test/blob/master/about_git.md#anc-github)  
改めて、秘密鍵の仕組みについて  
[参照url](https://persol-tech-s.co.jp/corporate/security/article.html?id=26)                     
ちなみに、秘密鍵にはいくつかフォーマットがあるそうです。  
id_rst、 id_rst.pem、 id_rst.ppk など。  
[参照url](http://www.puniokid.com/tips/linux/69/)  

3. git lab へSSH認証キーの設定  
[参照url](https://qiita.com/CUTBOSS/items/462a2ed28d264aeff7d5)  
4. sourcetree へSSH認証キーの設定  
gitlabは、sourcetreeのアカウント作成ができないので、こちらの方法を行ってください。  
[参照url](https://qiita.com/kyamawaki/items/05abbbec5f617f3ae5eb)

<br>
<br>

# git lab インターフェース  
日本語化できます。  

## 上部メニュー
1. プロジェクト（1サイトごとに、1プロジェクト）
        - starredプロジェクト 「star」を付けたプロジェクトを表示。良く使用するプロジェクトはstar付けておけばいいと思う。
2. グループ（プロジェクトをまとめておける）
        - 新規案件グループ、ms案件グループ、銀行グループなど　分類して、関係する人のみ、そのグループのメンバーする等

        

