# git lab CE （社内サーバーにインストールするプラン）について

- 社内サーバーにインストールする場合、Community Edition（CE）（gitlab-ce）プラン。  
- パッケージをインストールするだけ。
- 頻繁に更新されるので、なるべく変なカスタマイズはしない方がいい。
- ただ、特定の人に管理者になってもらう運用だと、その人がいなくなった場合にストップするというリスクもある。


## git lab CE（社内サーバー）インストール方法  
[参照url](https://www.creationline.com/lab/18684)    
[参照url](https://www.gitlab.jp/installation/)    


## 注意事項
- 32bitのマシンにはインストールできない。  
（会社の個人PCにて、virtulboxインストール、cent osインストールするも、32bitマシンのためgitlabをインストールできず。
自宅64bitマシンにてインストールできるかどうか確認。）
